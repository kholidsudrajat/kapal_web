<aside class="sidebar">
    <div class="col-xs-12 col-sm-12 col-md-4">
        <div class="widget">
            <div class="wi-side">
                <div class="wi-border">
                    <h3 class="wi-border-title">News</h3> </div>
                <div class="wi-content wi-about">
                    <div class="wi-about-slide owl-carousel owl-theme">
                        <div class="wi-about-item">
                            <div class="abt-img">
                                <img src='<?php echo base_url(); ?>asset/front/img/dummies/blog/65x65/thumb1.jpg' alt="About Photo">
                            </div>
                            <a href='#'>
                                <h4 class="gen-title">Lorem ipsum dolor sit </h4>
                            </a>
                            <p>Mazim alienum appellantur eu cu ullum officiis pro pri</p>
                        </div>
                        <!-- /.wi-about-item -->
                        <div class="wi-about-item">
                            <div class="abt-img">
                                <img src='<?php echo base_url(); ?>asset/front/img/dummies/blog/65x65/thumb2.jpg' alt="About Photo">
                            </div>
                            <a href='#'>
                                <h4 class="gen-title">Maiorum ponderum eum</h4>
                            </a>
                            <p>Mazim alienum appellantur eu cu ullum officiis pro pri</p>
                        </div>
                        <!-- /.wi-about-item -->
                        <div class="wi-about-item">
                            <div class="abt-img">
                                <img src='<?php echo base_url(); ?>asset/front/img/dummies/blog/65x65/thumb3.jpg' alt="About Photo">
                            </div>
                            <a href='#'>
                                <h4 class="gen-title">Et mei iusto dolorum</h4>
                            </a>
                            <p>Mazim alienum appellantur eu cu ullum officiis pro pri</p>
                        </div>
                        <!-- /.wi-about-item -->
                    </div>
                    <!-- /.wi-about-slide -->
                </div>
                <!-- /.wi-content -->
            </div>
            <!--     /.wi-side -->
        </div>
        <!-- /.widget -->
        <div class="widget">
            <div class="wi-side">
                <div class="wi-border">
                    <h3 class="wi-border-title">Categories</h3> </div>
                <div class="wi-content wi-cat">
                    <ul>
                        <li>
                            <a href="#"> <span>News</span> <span>(386)</span> </a>
                        </li>
                        <li>
                            <a href="#"> <span>Notice</span> <span>(6)</span> </a>
                        </li>
                        <li>
                            <a href="#"> <span>Event</span> <span>(3)</span> </a>
                        </li>
                        <li>
                            <a href="#"> <span>Press Release</span> <span>(8)</span> </a>
                        </li>
                    </ul>
                </div>
                <!-- /.wi-content -->
            </div>
            <!--     /.wi-side -->
        </div>
        <!-- /.widget -->
        <div class="widget">
            <div class="wi-side">
                <div class="wi-border">
                    <h3 class="wi-border-title">Search</h3> </div>
                <div class="wi-content wi-search">
                    <form action="#">
                        <div class="form-group">
                            <input type="search" placeholder="Search here.." class="form-control"> </div>
                        <div class="form-group">
                            <div class="search-btn">
                                <input type="submit" value="" class="form-control"> </div>
                        </div>
                    </form>
                </div>
                <!-- /.wi-content -->
            </div>
            <!--     /.wi-side -->
        </div>
        <!-- /.widget -->
        <div class="widget">
            <div class="wi-side">
                <div class="wi-border">
                    <h3 class="wi-border-title">Popular Post</h3> </div>
                <div class="wi-content wi-recent">
                    <ul>
                        <li><a href='detailberita-19.html'>Wärtsilä to power first CNG carrier</a></li>
                        <li><a href='detailberita-12.html'>IMO and Maritime Regulations Up Date</a></li>
                        <li><a href='detailberita-27.html'>DNV GL classes world’s largest boxship, MSC Oscar</a></li>
                        <li><a href='detailberita-10.html'>IMO MEETING OF SUB-COMMITTEE ON IMPLEMENTATION OF IMO INSTRUMENTS 1st SESSION</a></li>
                        <li><a href='detailberita-5.html'>DPP INSA Holds Gathering Event With BKI Board Of Director</a></li>
                    </ul>
                </div>
                <!-- /.wi-content -->
            </div>
            <!--     /.wi-side -->
        </div>
        <!-- /.widget -->
        <div class="widget">
            <div class="wi-side">
                <div class="wi-border">
                    <h3 class="wi-border-title">Tags</h3> </div>
                <div class="wi-content wi-tags">
                    <ul>
                        <li><a href="#">Mariitim</a></li>
                        <li><a href="#">Teknologi</a></li>
                        <li><a href="#">Bisnis</a></li>
                        <li><a href="#">Container</a></li>
                        <li><a href="#">Industry</a></li>
                        <li><a href="#">INSA</a></li>
                        <li><a href="#">Perhubungan Laut</a></li>
                        <li><a href="#">Edukasi</a></li>
                        <li><a href="#">Berpikir Rasional</a></li>
                        <li><a href="#">Olahraga</a></li>
                    </ul>
                </div>
                <!-- /.wi-content -->
            </div>
            <!--     /.wi-side -->
        </div>
        <!-- /.widget -->
    </div>
    <!-- /.col- -->
</aside>
<!-- /.sidebar -->
