<header class="clearfix header" data-spy="affix" data-offset-top="60">
    
    
    <div class="navbar navbar-default navbar-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <i class="fa fa-bars"></i>
                </button>
                <div class="logo">
                    <a class="navbar-brand" href="<?php echo base_url(); ?>">
                        <img alt="Logo" src='<?php echo base_url(); ?>asset/front/img/logo1.jpg' width='150'>
                    </a>
                </div>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <li class='last'><a class="" href="#">About Us</a>
                            <ul>
                                <li>
                                    <li class='last'><a class="" href="#">Company Profile</a>
                                        <ul>
                                            <li>
                                                <li class='last'><a href="<?php echo base_url();?>index.php/home/visi_misi">Vision and Mission</a></li>
                                                <li class='last'><a href="<?php echo base_url();?>index.php/home/moto">Company Moto</a></li>
                                                <li class='last'><a href="<?php echo base_url();?>index.php/home/history">History</a></li>
                                                <li class='last'><a href="<?php echo base_url();?>index.php/home/contact">Contact</a></li>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class='last'><a href="<?php echo base_url();?>index.php/home/achivment">Achivment</a></li>
                                    <li class='last'><a href="<?php echo base_url();?>index.php/home/quality_policy">Quality Policy</a></li>
                                </li>
                            </ul>
                        </li>
                        <li><a href="<?php echo base_url();?>index.php/home/our_product">Our Product</a></li>
                        <li class='last'>
                            <a class="" href="#">Gallery</a>
                            <ul>
                                <li>
                                    <li class='last'><a href="<?php echo base_url();?>index.php/home/our_photos">Our Photos</a></li>
                                    <li class='last'><a href="<?php echo base_url();?>index.php/home/our_videos">Our Videos</a></li>
                                </li>
                            </ul>
                        </li>
                        <li><a href="<?php echo base_url();?>index.php/home/article">Aticle</a></li>
                        <li><a href="<?php echo base_url();?>index.php/home/news">News</a></li>
                        <li><a href="<?php echo base_url();?>index.php/home/career">Career</a></li>
                    </li>
                </ul>
                <!-- End Navigation List -->
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
        <!-- Mobile Menu Start -->
        <ul class="wpb-mobile-menu">
            <li> <a href="#">About Us</a>
                <ul class="dropdown">
                    <!--<li><a href="halamanstatis-63.html">Company Profile</a></li>-->
                    <li><a href="<?php echo base_url();?>index.php/home/visi_misi">Vision and Mission</a></li>
                    <li><a href="<?php echo base_url();?>index.php/home/moto">Company Moto</a></li>
                    <li><a href="<?php echo base_url();?>index.php/home/history">History</a></li>
                    <li><a href="<?php echo base_url();?>index.php/home/contact">Contact</a></li>
                    <li><a href="<?php echo base_url();?>index.php/home/achivment">Achivment</a></li>
                    <li><a href="<?php echo base_url();?>index.php/home/quality_policy">Quality Policy</a></li>
                </ul>
            </li>
            <li><a href="<?php echo base_url();?>index.php/home/our_product">Our Product</a></li>
            <li> <a href="#">Gallery</a>
                <ul class="dropdown">
                    <li><a href="<?php echo base_url();?>index.php/home/our_photos">Our Photos</a></li>
                    <li><a href="<?php echo base_url();?>index.php/home/our_videos">Our Videos</a></li>
                </ul>
            </li>
            <li><a href="<?php echo base_url();?>index.php/home/career">Career</a></li>
        </ul>
        <!-- /.wpb-mobile-menu -->
    </div>
    
</header>