<head>
    <meta charset="utf-8">
    <title>Sailor - Bootstrap 3 corporate template</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Bootstrap 3 template for corporate business" />
    <!-- css -->
    <link href="<?php echo base_url(); ?>asset/front/css/bootstrap.min.css" rel="stylesheet" />
    
    <!-- Font Awesome CSS -->
    <link rel="stylesheet" href='<?php echo base_url(); ?>asset/front/css/font-awesome.min.css' type="text/css" />
    <!-- Slicknav -->
    <link rel="stylesheet" href='<?php echo base_url(); ?>asset/front/css/slicknav.css' type="text/css" />
    <!-- Owl Carousel -->
    <link rel="stylesheet" href='<?php echo base_url(); ?>asset/front/css/owl.carousel.min.css' type="text/css" />
    <link rel="stylesheet" href='<?php echo base_url(); ?>asset/front/css/owl.theme.default.min.css' type="text/css" />
    <!-- Css3 Transitions Styles  -->
    <link rel="stylesheet" href='<?php echo base_url(); ?>asset/front/css/animate.css' type="text/css" />
    <!-- FancyBox -->
    <link rel="stylesheet" href='<?php echo base_url(); ?>asset/front/css/jquery.fancybox.min.css' type="text/css" />
    <!-- Slick Slider -->
    <link rel="stylesheet" type="text/css" href='<?php echo base_url(); ?>asset/front/css/slick.css' />
    <link rel="stylesheet" type="text/css" href='<?php echo base_url(); ?>asset/front/css/slick-theme.css' />
    <!-- Main CSS   -->
    <link rel="stylesheet" href='<?php echo base_url(); ?>asset/front/css/style.css' type="text/css" />
    <!-- Responsive CSS  -->
    <link rel="stylesheet" href='<?php echo base_url(); ?>asset/front/css/responsive.css' type="text/css" />
    <!-- Portofolio -->
    <!--<link href="<?php echo base_url(); ?>asset/front/css/cubeportfolio.min.css" rel="stylesheet" />-->

    <!-- Theme skin -->
    <link id="t-colors" href="<?php echo base_url(); ?>asset/front/skins/default.css" rel="stylesheet" />
    <link href='http://fonts.googleapis.com/css?family=Merriweather:400,700,400italic' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

    <!-- boxed bg -->
    <link id="bodybg" href="<?php echo base_url(); ?>asset/front/bodybg/bg1.css" rel="stylesheet" type="text/css" />
</head>