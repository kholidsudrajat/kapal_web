<!-- start slider -->			
<!-- Slider -->
<section id="main-slider">
    <!-- Carousel -->
    <div id="main-slide" class="main-slider carousel slide" data-ride="carousel">
        <!-- Carousel inner -->
        <div class="carousel-inner">
            <div class="item active">
                <img class="img-responsive" src='<?php echo base_url(); ?>asset/front/img/slides/flexslider/1.jpg' alt='Classification Services'>
                <div class="container">
                    <div class="slider-content">
                        <div class="container">
                            <h2 class="animated4">
                                <span>Classification Services</span>
                            </h2>
                            <p class="animated6 slide-para">
                                <span>Improve Marine Safety Standard</span>
                            </p>
                            <p class="animated4">
                                <a href="#" class="slider-btn cbiz-btn">Readmore</a>
                            </p>
                        </div>
                    </div>
                </div>
                <!-- /.container -->
            </div>
            <!-- /.item -->
            <div class="item">
                <img class="img-responsive" src='<?php echo base_url(); ?>asset/front/img/slides/flexslider/2.jpg' alt='Commercial Services'>
                <div class="container">
                    <div class="slider-content">
                        <div class="container">
                            <h2 class="animated4">
                                <span>Commercial Services</span>
                            </h2>
                            <p class="animated6 slide-para">
                                <span>Safety Assurance</span>
                            </p>
                            <p class="animated4">
                                <a href="#" class="slider-btn cbiz-btn">Readmore</a>
                            </p>
                        </div>
                    </div>
                </div>
                <!-- /.container -->
            </div>
            <!-- /.item -->
            <div class="item">
                <img class="img-responsive" src='<?php echo base_url(); ?>asset/front/img/slides/flexslider/3.jpg' alt='Statutory Services'>
                <div class="container">
                    <div class="slider-content">
                        <div class="container">
                            <h2 class="animated4">
                                <span>Statutory Services</span>
                            </h2>
                            <p class="animated6 slide-para">
                                <span>Marine Environment Protection</span>
                            </p>
                            <p class="animated4">
                                <a href="#" class="slider-btn cbiz-btn">Readmore</a>
                            </p>
                        </div>
                    </div>
                </div>
                <!-- /.container -->
            </div>
            
        </div>
        <!-- Carousel inner end-->
        <!-- Controls -->
        <a class="left carousel-control" href="#main-slide" data-slide="prev">
            <span><i class="fa fa-angle-left"></i></span>
        </a>
        <a class="right carousel-control" href="#main-slide" data-slide="next">
            <span><i class="fa fa-angle-right"></i></span>
        </a>
        <div class="click-to-next bottom-top-animation">
            <i></i>
        </div>
    </div>
    <!-- /carousel -->
</section>

<section class="service section">
    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-xs-12 col-sm-offset-2 col-xs-offset-0">
                <div class="section-heading">
                    <h2 class="title"><span></span> </h2>
                    <p></p>
                </div>
            </div>
        </div>
        <!-- /.row -->
        <div class='row'>
            <div class="col-md-4 col-sm-6 service-box" data-animation="fadeIn" data-animation-delay="01">
                <div class="service-icon">
                    <img class="flaticon-img icon-large" src="<?php echo base_url(); ?>asset/front/img/img_shortcut/27679801-ship.jpg" alt="Icon">
                </div>
                <div class="service-content">
                    <a href="#"><h4>Ship Register</h4></a>
                    <p>
                        <p>Vessel&#39;s Under IMCS Class</p>
                    </p>
                </div>
            </div>
            <!-- /.service-box -->
            <div class="col-md-4 col-sm-6 service-box" data-animation="fadeIn" data-animation-delay="01">
                <div class="service-icon">
                    <img class="flaticon-img icon-large" src="<?php echo base_url(); ?>asset/front/img/img_shortcut/54178202-armada.jpg" alt="Icon">
                </div>
                <div class="service-content">
                    <a href="#"><h4>IMCS Armada</h4></a>
                    <p>
                        <p>Information for Owner or Operator Vessel Under IMCS Class</p>
                    </p>
                </div>
            </div>
            <!-- /.service-box -->
            <div class="col-md-4 col-sm-6 service-box" data-animation="fadeIn" data-animation-delay="01">
                <div class="service-icon">
                    <img class="flaticon-img icon-large" src="<?php echo base_url(); ?>asset/front/img/img_shortcut/40509903-rule.jpg" alt="Icon">
                </div>
                <div class="service-content">
                    <a href="#"><h4>Rules, Regulation and Guidelines</h4></a>
                    <p>
                        <p>Rules, Regulation & Guidelines</p>
                    </p>
                </div>
            </div>
            <!-- /.service-box -->
        </div>
        <div class='row'>
            <div class="col-md-4 col-sm-6 service-box" data-animation="fadeIn" data-animation-delay="01">
                <div class="service-icon">
                    <img class="flaticon-img icon-large" src="<?php echo base_url(); ?>asset/front/img/img_shortcut/33551505-teckjurnal.jpg" alt="Icon">
                </div>
                <div class="service-content">
                    <a href="#"><h4>Technical Journal</h4></a>
                    <p>
                        <p>Technical Journal IMCS</p>
                    </p>
                </div>
            </div>
            <!-- /.service-box -->
            <div class="col-md-4 col-sm-6 service-box" data-animation="fadeIn" data-animation-delay="01">
                <div class="service-icon">
                    <img class="flaticon-img icon-large" src="<?php echo base_url(); ?>asset/front/img/img_shortcut/31768207-sertifikat.jpg" alt="Icon">
                </div>
                <div class="service-content">
                    <a href="#"><h4>Certificate Verification</h4></a>
                    <p>
                        <p>Verification Certificate Validity</p>
                    </p>
                </div>
            </div>
            <!-- /.service-box -->
            <div class="col-md-4 col-sm-6 service-box" data-animation="fadeIn" data-animation-delay="01">
                <div class="service-icon">
                    <img class="flaticon-img icon-large" src="<?php echo base_url(); ?>asset/front/img/img_shortcut/97582606-teckinfo.jpg" alt="Icon">
                </div>
                <div class="service-content">
                    <a href="#"><h4>Manufacturer & Service Supplier Approval</h4></a>
                    <p>
                        <p>Manufacturer & Service Supplier Approval</p>
                    </p>
                </div>
            </div>
            <!-- /.service-box -->
        </div>
        <!-- /.row -->
    </div>
    <!-- .container -->
</section>

<section class="our-goal section cbiz-bg">
    <div class="container">
        <div class="section-heading">
            <h2 class="title"><span>Our</span> Goal</h2>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6">
                <div id="goal-slider" class="carousel goal-slider slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#goal-slider" data-slide-to="0" class="active"></li>
                        <li data-target="#goal-slider" data-slide-to="1"></li>
                        <li data-target="#goal-slider" data-slide-to="2"></li>
                    </ol>
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <div class="item active">
                            <div class="goal-content">
                                <p>Visions 2016 -2020:
                                    <br> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua
                                </p>
                                <br>
                                <p>Missions 2016 -2020:
                                    <br> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                            </div>
                        </div>
                        <!-- /.item -->
                        <div class="item">
                            <div class="goal-content">
                                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium:
                                    <br> 1. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua
                                    <br> 2. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                                    <br> 3. Duis aute irure dolor in reprehenderit in voluptate velit esse </p>
                                <br>
                                <p></p>
                            </div>
                        </div>
                        <!-- /.item -->
                        <div class="item">
                            <div class="goal-content">
                                <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est:
                                    <br> 1. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua
                                    <br> 2. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                                    <br> 3. Duis aute irure dolor in reprehenderit in voluptate velit esse </p>
                                <br>
                                <p></p>
                            </div>
                        </div>
                        <!-- /.item -->
                    </div>
                    <!-- /.carousel-inner -->
                </div>
                <!-- /carousel -->
            </div>
            <!-- /.col- -->
            <div class="col-xs-12 col-sm-12 col-md-6">
                <div class="row">
                    <div class="projects">
                        <div class="col-xs-6">
                            <div class="pro-item">
                                <h2 class="counter">12254</h2> <a href="#"><h3 class="achievement">Ship Register</h3></a>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="pro-item">
                                <h2 class="counter">2</h2> <a href="#"><h3 class="achievement">Strategic Business Unit</h3></a>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="pro-item">
                                <h2 class="counter">36</h2> <a href="map.html"><h3 class="achievement">Branches</h3></a>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="pro-item">
                                <h2 class="counter">5120</h2> <a href="#"><h3 class="achievement">Client</h3></a>
                            </div>
                        </div>
                    </div>
                    <!-- /.projects -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.col- -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</section>

<section class="portfolios section">
    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-xs-12 col-sm-offset-2 col-xs-offset-0">
                <div class="section-heading">
                    <h2 class="title"><span>Our</span> Gallery</h2>
                    <p></p>
                </div>
            </div>
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="portfolio-details">
                <div class="portfolio-container grid">
                    <div class="grid-item col-md-4 col-xs-6 figts limited">
                        <div class="portfolio-item">
                            <img class="img-responsive" src='<?php echo base_url(); ?>asset/front/img/img_galeri/202574DSC_6886.JPG' alt="Gallery">
                            <div class="port-hover">
                                <div class="port-content">
                                    <a href="#"><h3>Activity</h3></a>
                                    <p>GLAD - February 2018</p>
                                    <a href='<?php echo base_url(); ?>asset/front/img/img_galeri/202574DSC_6886.JPG' data-fancybox="group" class="port-icon">
                                        <img class="hidden-thumbnail" src='<?php echo base_url(); ?>asset/front/img/img_galeri/202574DSC_6886.JPG' alt="Thumbnail">
                                        <img src='<?php echo base_url(); ?>asset/front/img/icons/plus-btn.png' alt="Icon">
                                    </a>
                                </div>
                                <!-- /.port-content -->
                            </div>
                            <!-- /.port-hover -->
                        </div>
                        <!-- /.portfolio-item -->
                    </div>
                    <!-- /.grid-item -->
                    <div class="grid-item col-md-4 col-xs-6 figts limited">
                        <div class="portfolio-item">
                            <img class="img-responsive" src='<?php echo base_url(); ?>asset/front/img/img_galeri/202985DSC_6813.JPG' alt="Gallery">
                            <div class="port-hover">
                                <div class="port-content">
                                    <a href="#"><h3>Activity</h3></a>
                                    <p>BKI-PINDAD Mou</p>
                                    <a href='<?php echo base_url(); ?>asset/front/img/img_galeri/202985DSC_6813.JPG' data-fancybox="group" class="port-icon">
                                        <img class="hidden-thumbnail" src='<?php echo base_url(); ?>asset/front/img/img_galeri/202985DSC_6813.JPG' alt="Thumbnail">
                                        <img src='<?php echo base_url(); ?>asset/front/img/icons/plus-btn.png' alt="Icon">
                                    </a>
                                </div>
                                <!-- /.port-content -->
                            </div>
                            <!-- /.port-hover -->
                        </div>
                        <!-- /.portfolio-item -->
                    </div>
                    <!-- /.grid-item -->
                    <div class="grid-item col-md-4 col-xs-6 figts limited">
                        <div class="portfolio-item">
                            <img class="img-responsive" src='<?php echo base_url(); ?>asset/front/img/img_galeri/769839DSC_6365.JPG' alt="Gallery">
                            <div class="port-hover">
                                <div class="port-content">
                                    <a href="#"><h3>Activity</h3></a>
                                    <p>Gala Dinner</p>
                                    <a href='<?php echo base_url(); ?>asset/front/img/img_galeri/769839DSC_6365.JPG' data-fancybox="group" class="port-icon">
                                        <img class="hidden-thumbnail" src='<?php echo base_url(); ?>asset/front/img/img_galeri/769839DSC_6365.JPG' alt="Thumbnail">
                                        <img src='<?php echo base_url(); ?>asset/front/img/icons/plus-btn.png' alt="Icon">
                                    </a>
                                </div>
                                <!-- /.port-content -->
                            </div>
                            <!-- /.port-hover -->
                        </div>
                        <!-- /.portfolio-item -->
                    </div>
                    <!-- /.grid-item -->
                    <div class="grid-item col-md-4 col-xs-6 figts limited">
                        <div class="portfolio-item">
                            <img class="img-responsive" src='<?php echo base_url(); ?>asset/front/img/img_galeri/872522.jpeg' alt="Gallery">
                            <div class="port-hover">
                                <div class="port-content">
                                    <a href="#"><h3>Activity</h3></a>
                                    <p>Statutory Survey Socialization</p>
                                    <a href='<?php echo base_url(); ?>asset/front/img/img_galeri/872522.jpeg' data-fancybox="group" class="port-icon">
                                        <img class="hidden-thumbnail" src='<?php echo base_url(); ?>asset/front/img/img_galeri/872522.jpeg' alt="Thumbnail">
                                        <img src='<?php echo base_url(); ?>asset/front/img/icons/plus-btn.png' alt="Icon">
                                    </a>
                                </div>
                                <!-- /.port-content -->
                            </div>
                            <!-- /.port-hover -->
                        </div>
                        <!-- /.portfolio-item -->
                    </div>
                    <!-- /.grid-item -->
                    <div class="grid-item col-md-4 col-xs-6 figts limited">
                        <div class="portfolio-item">
                            <img class="img-responsive" src='<?php echo base_url(); ?>asset/front/img/img_galeri/63059.jpeg' alt="Gallery">
                            <div class="port-hover">
                                <div class="port-content">
                                    <a href="#"><h3>Activity</h3></a>
                                    <p>Ship Launching</p>
                                    <a href='<?php echo base_url(); ?>asset/front/img/img_galeri/63059.jpeg' data-fancybox="group" class="port-icon">
                                        <img class="hidden-thumbnail" src='<?php echo base_url(); ?>asset/front/img/img_galeri/63059.jpeg' alt="Thumbnail">
                                        <img src='<?php echo base_url(); ?>asset/front/img/icons/plus-btn.png' alt="Icon">
                                    </a>
                                </div>
                                <!-- /.port-content -->
                            </div>
                            <!-- /.port-hover -->
                        </div>
                        <!-- /.portfolio-item -->
                    </div>
                    <!-- /.grid-item -->
                    <div class="grid-item col-md-4 col-xs-6 figts limited">
                        <div class="portfolio-item">
                            <img class="img-responsive" src='<?php echo base_url(); ?>asset/front/img/img_galeri/855394.jpeg' alt="Gallery">
                            <div class="port-hover">
                                <div class="port-content">
                                    <a href="#"><h3>CSR</h3></a>
                                    <p>Social Responsibility</p>
                                    <a href='<?php echo base_url(); ?>asset/front/img/img_galeri/855394.jpeg' data-fancybox="group" class="port-icon">
                                        <img class="hidden-thumbnail" src='<?php echo base_url(); ?>asset/front/img/img_galeri/855394.jpeg' alt="Thumbnail">
                                        <img src='<?php echo base_url(); ?>asset/front/img/icons/plus-btn.png' alt="Icon">
                                    </a>
                                </div>
                                <!-- /.port-content -->
                            </div>
                            <!-- /.port-hover -->
                        </div>
                        <!-- /.portfolio-item -->
                    </div>
                    <!-- /.grid-item -->
                    <div class="grid-item col-md-4 col-xs-6 figts limited">
                        <div class="portfolio-item">
                            <img class="img-responsive" src='<?php echo base_url(); ?>asset/front/img/img_galeri/122517DSC_5745.JPG' alt="Gallery">
                            <div class="port-hover">
                                <div class="port-content">
                                    <a href="#"><h3>Activity</h3></a>
                                    <p>Seminar of IMO Meeting Result</p>
                                    <a href='<?php echo base_url(); ?>asset/front/img/img_galeri/122517DSC_5745.JPG' data-fancybox="group" class="port-icon">
                                        <img class="hidden-thumbnail" src='<?php echo base_url(); ?>asset/front/img/img_galeri/122517DSC_5745.JPG' alt="Thumbnail">
                                        <img src='<?php echo base_url(); ?>asset/front/img/icons/plus-btn.png' alt="Icon">
                                    </a>
                                </div>
                                <!-- /.port-content -->
                            </div>
                            <!-- /.port-hover -->
                        </div>
                        <!-- /.portfolio-item -->
                    </div>
                    <!-- /.grid-item -->
                    <div class="grid-item col-md-4 col-xs-6 figts limited">
                        <div class="portfolio-item">
                            <img class="img-responsive" src='<?php echo base_url(); ?>asset/front/img/img_galeri/135580DSC_5628.JPG' alt="Gallery">
                            <div class="port-hover">
                                <div class="port-content">
                                    <a href="#"><h3>Activity</h3></a>
                                    <p>Inauguration of The Commissioner</p>
                                    <a href='<?php echo base_url(); ?>asset/front/img/img_galeri/135580DSC_5628.JPG' data-fancybox="group" class="port-icon">
                                        <img class="hidden-thumbnail" src='<?php echo base_url(); ?>asset/front/img/img_galeri/135580DSC_5628.JPG' alt="Thumbnail">
                                        <img src='<?php echo base_url(); ?>asset/front/img/icons/plus-btn.png' alt="Icon">
                                    </a>
                                </div>
                                <!-- /.port-content -->
                            </div>
                            <!-- /.port-hover -->
                        </div>
                        <!-- /.portfolio-item -->
                    </div>
                    <!-- /.grid-item -->
                    <div class="grid-item col-md-4 col-xs-6 figts limited">
                        <div class="portfolio-item">
                            <img class="img-responsive" src='<?php echo base_url(); ?>asset/front/img/img_galeri/4411DSC_5063.JPG' alt="Gallery">
                            <div class="port-hover">
                                <div class="port-content">
                                    <a href="#"><h3>Activity</h3></a>
                                    <p>Campus Visits</p>
                                    <a href='<?php echo base_url(); ?>asset/front/img/img_galeri/4411DSC_5063.JPG' data-fancybox="group" class="port-icon">
                                        <img class="hidden-thumbnail" src='<?php echo base_url(); ?>asset/front/img/img_galeri/4411DSC_5063.JPG' alt="Thumbnail">
                                        <img src='<?php echo base_url(); ?>asset/front/img/icons/plus-btn.png' alt="Icon">
                                    </a>
                                </div>
                                <!-- /.port-content -->
                            </div>
                            <!-- /.port-hover -->
                        </div>
                        <!-- /.portfolio-item -->
                    </div>
                    <!-- /.grid-item -->
                </div>
                <!-- /.portfolio-container -->
            </div>
            <!-- /.portfolio-details -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</section>

<div class="our-client">
    <div class="container">
        <div class="client-logos">
            <div class="client-slider owl-carousel owl-theme">
                <div class="item">
                    <div class="logo-each">
                        <a href='#'> <img src='<?php echo base_url(); ?>asset/front/img/clients/acs.jpg' title='Client' alt="Client Logo"> </a>
                    </div>
                </div>
                <div class="item">
                    <div class="logo-each">
                        <a href='http://dephub.go.id/'> <img src='<?php echo base_url(); ?>asset/front/img/clients/bumn.jpg' title='Client' alt="Client Logo"> </a>
                    </div>
                </div>
                <div class="item">
                    <div class="logo-each">
                        <a href='http://asiancs.org'> <img src='<?php echo base_url(); ?>asset/front/img/clients/darmapersada.jpg' title='Client' alt="Client Logo"> </a>
                    </div>
                </div>
                <div class="item">
                    <div class="logo-each">
                        <a href='http://insa.or.id'> <img src='<?php echo base_url(); ?>asset/front/img/clients/ptsabda.jpg' title='Client' alt="Client Logo"> </a>
                    </div>
                </div>
                <div class="item">
                    <div class="logo-each">
                        <a href='http://iperindo.org'> <img src='<?php echo base_url(); ?>asset/front/img/clients/dephub.jpg' title='Client' alt="Client Logo"> </a>
                    </div>
                </div>
                <div class="item">
                    <div class="logo-each">
                        <a href='http://www.indonesiaport.co.id'> <img src='<?php echo base_url(); ?>asset/front/img/clients/logo6.png' title='Client' alt="Client Logo"> </a>
                    </div>
                </div>
                <div class="item">
                    <div class="logo-each">
                        <a href='#'> <img src='<?php echo base_url(); ?>asset/front/img/clients/acs.jpg' title='Client' alt="Client Logo"> </a>
                    </div>
                </div>
                <div class="item">
                    <div class="logo-each">
                        <a href='http://dephub.go.id/'> <img src='<?php echo base_url(); ?>asset/front/img/clients/bumn.jpg' title='Client' alt="Client Logo"> </a>
                    </div>
                </div>
                <div class="item">
                    <div class="logo-each">
                        <a href='http://asiancs.org'> <img src='<?php echo base_url(); ?>asset/front/img/clients/darmapersada.jpg' title='Client' alt="Client Logo"> </a>
                    </div>
                </div>
                <div class="item">
                    <div class="logo-each">
                        <a href='http://insa.or.id'> <img src='<?php echo base_url(); ?>asset/front/img/clients/ptsabda.jpg' title='Client' alt="Client Logo"> </a>
                    </div>
                </div>
                <div class="item">
                    <div class="logo-each">
                        <a href='http://iperindo.org'> <img src='<?php echo base_url(); ?>asset/front/img/clients/dephub.jpg' title='Client' alt="Client Logo"> </a>
                    </div>
                </div>
                <div class="item">
                    <div class="logo-each">
                        <a href='http://www.indonesiaport.co.id'> <img src='<?php echo base_url(); ?>asset/front/img/clients/logo6.png' title='Client' alt="Client Logo"> </a>
                    </div>
                </div>
            </div>
            <!-- /.client-slider -->
        </div>
        <!-- /.client-logos -->
    </div>
    <!-- /.container -->
</div>