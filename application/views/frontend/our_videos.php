<section class="portfolios section port-detail">
    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-xs-12 col-sm-offset-2 col-xs-offset-0">
                <div class="section-heading">
                    <h2 class="title"><span>Our</span> Video</h2>
                    <p>.</p>
                </div>
            </div>
        </div>
        <!-- /.row -->
        <!--<div class="portfolio-filter">
            <a class="current" href="#" data-filter="*">All</a>
        </div>-->
        
        <div class="row">
            <div class="portfolio-details">
                <div class="portfolio-container grid">
                    <div class="grid-item col-md-4 col-xs-6 figts limited">
                        <div class="portfolio-item">
                            <iframe width="100%" src="https://www.youtube.com/embed/L3J59X9jg2M" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                        <!-- /.portfolio-item -->
                    </div>
                    <!-- /.grid-item -->
                    <div class="grid-item col-md-4 col-xs-6 figts limited">
                        <div class="portfolio-item">
                            <iframe width="100%" src="https://www.youtube.com/embed/7w0JlPtdYNE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                        <!-- /.portfolio-item -->
                    </div>
                    <div class="grid-item col-md-4 col-xs-6 figts limited">
                        <div class="portfolio-item">
                            <iframe width="100%" src="https://www.youtube.com/embed/L3J59X9jg2M" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                        <!-- /.portfolio-item -->
                    </div>
                    <!-- /.grid-item -->
                    <div class="grid-item col-md-4 col-xs-6 figts limited">
                        <div class="portfolio-item">
                            <iframe width="100%" src="https://www.youtube.com/embed/7w0JlPtdYNE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                        <!-- /.portfolio-item -->
                    </div>
                    <div class="grid-item col-md-4 col-xs-6 figts limited">
                        <div class="portfolio-item">
                            <iframe width="100%" src="https://www.youtube.com/embed/L3J59X9jg2M" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                        <!-- /.portfolio-item -->
                    </div>
                    <!-- /.grid-item -->
                    <div class="grid-item col-md-4 col-xs-6 figts limited">
                        <div class="portfolio-item">
                            <iframe width="100%" src="https://www.youtube.com/embed/7w0JlPtdYNE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                        <!-- /.portfolio-item -->
                    </div>
                    <!-- /.grid-item -->
                </div>
                <!-- /.portfolio-container -->
            </div>
            <!-- /.portfolio-details -->
        </div>
        
    </div>
</section>