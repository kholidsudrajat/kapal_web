<h4>History</h4>
<ul class="nav nav-tabs">
    <li class="active"><a href="#one" data-toggle="tab"><i class="icon-briefcase"></i> 1970</a></li>
    <li><a href="#two" data-toggle="tab">1980</a></li>
    <li><a href="#three" data-toggle="tab">2000</a></li>
    <li><a href="#four" data-toggle="tab">2016</a></li>
</ul>
<div class="tab-content">
    <div class="tab-pane active" id="one">
        <p>
            <strong>Augue iriure</strong> dolorum per ex, ne iisque ornatus veritus duo. Ex nobis integre lucilius sit, pri ea falli ludus appareat. Eum quodsi fuisset id, nostro patrioque qui id. Nominati eloquentiam in mea.
        </p>
        <p>
            No eum sanctus vituperata reformidans, dicant abhorreant ut pro. Duo id enim iisque praesent, amet intellegat per et, solet referrentur eum et.
        </p>
    </div>
    <div class="tab-pane" id="two">
        <p>
            Tale dolor mea ex, te enim assum suscipit cum, vix aliquid omittantur in. Duo eu cibo dolorum menandri, nam sumo dicit admodum ei. Ne mazim commune honestatis cum, mentitum phaedrum sit et.
        </p>
    </div>
    <div class="tab-pane" id="three">
        <p>
            Cu cum commodo regione definiebas. Cum ea eros laboramus, audire deseruisse his at, munere aeterno ut quo. Et ius doming causae philosophia, vitae bonorum intellegat usu cu.
        </p>
    </div>
    <div class="tab-pane" id="four">
        <p>
            Cu cum commodo regione definiebas. Cum ea eros laboramus, audire deseruisse his at, munere aeterno ut quo. Et ius doming causae philosophia, vitae bonorum intellegat usu cu.
        </p>
    </div>
</div>