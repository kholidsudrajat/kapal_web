<section class="page-head page-bg" style="background-image: url('<?php echo base_url(); ?>asset/front/img/slides/flexslider/2.jpg')">
    <div class="container">
        <h3 class="page-heading">Articles</h3>
        <div class="sub-title">
            <a href="<?php echo base_url(); ?>"><span>Home</span></a>
            <i class="fa fa-angle-right"></i>
            <span>Articles</span>
        </div>
    </div>
</section>
<section id="content" style="padding: 0 0 20px; border-bottom: 1px solid #ccc;" class="zebra">
    <section class="grid blocks listing">
        <section class="grid filterset">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="grid12">Filter</h2>
                        <div>
                            <div class="block">
                                <h3>Select a product</h3>

                                <select id="ddlNewsCluster">
                                    <option selected>Select a product</option>

                                    <option value="406A005BF56A480A86285B52AB325939">Accommodation Support Vessel</option>

                                    <option value="F7C1B157ADAA45A4991FA0D5D61F6DA2">Auxiliaries</option>

                                    <option value="E747C3C2830E4FF597889FB8FE03A17E">Barges</option>

                                    <option value="43EF947A61CE4B7EBC5D3086F2DF2427">Booster Station</option>

                                    <option value="B041AD02769E46158DE5B1EC6ED13728">Cablelay Vessel</option>

                                    <option value="5BED4A10379349649134CECA1915C6A2">Combatants</option>

                                    <option value="F21D741A2E794EFCA213827709DC8216">Combi Coaster</option>

                                    <option value="A82F98FA2187429BA7E3A0C1275B7C64">Container Vessel</option>

                                    <option value="85051455EB8A42BC8228E13643DE2C58">Crew Supply Vessel</option>

                                    <option value="56BE6D8E304C40B98E1D70C5CEC30D2F">Custom Built</option>

                                    <option value="554EE3E489C649B3A55799458778DD49">Cutter Suction Dredger</option>

                                    <option value="78692BBDA5D34E0189BD07AD9D4E4DCA">DOP Dredger</option>

                                    <option value="E1380C51D9C14602AC8ABB502ED150FC">DOP Pump</option>

                                    <option value="958E0F89F63C4D2D827DCBFC71EE57A0">Dry Cargo Vessel</option>

                                    <option value="4B371B8337A2455A92AAF087DCB6805F">Escort Tug</option>

                                    <option value="C3CCE2BC477B4810985D7B144FDC762F">Fire Fighting Vessel</option>

                                    <option value="71090C89B8D74EE1AF68F634B193F7FF">Fishing Vessels</option>

                                    <option value="C2EAAA6A039045EEB83976FAE2E6E436">Frigate</option>

                                    <option value="0EDB7A8810124A38A3C4A69A5B1E2784">Harbour Tugs</option>

                                    <option value="4AF09B701FE64794B6612DC4B8B31E8A">Heavy Lift Load Vessel Offshore</option>

                                    <option value="1C309AFAD9C84AA8AD33EE36D9143752">Interceptor</option>

                                    <option value="FD27D2D72B034B87A84B543824206A36">INVASAVE</option>

                                    <option value="A202FAF62FF9428DBD31D1478D26ABBE">Liquefied Gas Carrier</option>

                                    <option value="FF02512AE11B4370B4835EDAD2C03082">Modular Vessel</option>

                                    <option value="37944ED9C47D4372955311596EAABB4E">Multi Purpose Vessel Offshore</option>

                                    <option value="72369D778B4D47D6ABF44926118EA4BA">Multi Purpose Vessel Shipping</option>

                                    <option value="F85443F5AD064BB6AAEDEDD116FB14C2">Offshore Patrol Vessel</option>

                                    <option value="E74037C988D44B54A494E530A3730948">Offshore Support Vessel</option>

                                    <option value="6850B544E0C84D068810D27FE527F87A">Oil Tanker</option>

                                    <option value="3A0CBA3D6CA741678B8C3BFFB327C088">Passenger Car Ferry</option>

                                    <option value="8A509394C0404E35AF39277F511480E0">Passenger Ferry</option>

                                    <option value="0B9B828EFAA249CDAAAC49B0EEAF3F54">Patrol Vessel</option>

                                    <option value="5B781AE2A4ED4288A17F361296EC0D4D">Pilot Vessel</option>

                                    <option value="90237A52B007452380A8B43FDEE84505">Platform Supply Vessels</option>

                                    <option value="0C0EB2895428449A98558F6217F3320A">Pontoon</option>

                                    <option value="7B2DDC619BE0430086B57F87CC095383">Research Vessel</option>

                                    <option value="F2E4AB11FCE749C787173718BE9C9898">Rigid Hull Inflatable Boat</option>

                                    <option value="5D938129E177481FBCC0493B025F19A9">Sea Cruising</option>

                                    <option value="968F0D2983C64912B16DDD76EC355B17">Sea Tug</option>

                                    <option value="F488C5AD9C5440ABBABAFF42D7C06A52">Search and Rescue Vessel</option>

                                    <option value="36FA22B4E4C247AF8A2D69937801F06B">SeaXplorer</option>

                                    <option value="3611C0344787478F9CC3592CE5AEEBC7">Stan Patrol</option>

                                    <option value="1D82FE2165FB496683C3854EC64A21AB">Superyachts</option>

                                    <option value="3211E375061D44BCB15531D7E4460156">Trailing Suction Hopper Dredgers</option>

                                    <option value="6BE547F9B2FE4463A2547232CAE7A896">Training Vessel</option>

                                    <option value="006C0C1E34354F5987D4E023C6E7F825">Utility Support Vessel</option>

                                    <option value="C083129367EC4E6D999964D73A34CD54">Water Injection Dredgers</option>

                                    <option value="169767A321514903ABEEFE0CA69C87D6">Yacht Support Vessel</option>

                                </select>

                            </div>
                            <div class="block">
                                <h3>Select a subject</h3>

                                <select id="ddlNewsSubject">
                                    <option selected>Select a subject</option>

                                    <option value="9922F946A66C418A8CEA2639CAAF1AFF">Arrival</option>

                                    <option value="40FEA1B4372541ADA16C63767E0896F6">Award</option>

                                    <option value="736CDD96527C4942A0A4B33893ACAC51">Certification</option>

                                    <option value="D8A32704B6354D9A85857DE6AC7D4420">Christening</option>

                                    <option value="E701F66E53F642FAA579DA1F9D7AE47F">Completion</option>

                                    <option value="BF412C40D54E4C6D83B911794153F21F">Components</option>

                                    <option value="08BE6D248C8F47879EB294A73CB26A53">Contract</option>

                                    <option value="4462091E60394E13888B516AF5FDB9E2">Conversion</option>

                                    <option value="B4D8607A8E8045DD85BB275FAFA0A121">Customer update</option>

                                    <option value="7EDDF0A90C72451C9E55C120702CFEAD">Delivery</option>

                                    <option value="5E26E4B8E56B445B93EC96B7A83CDAF7">Event</option>

                                    <option value="A0EE38A06CF34E279B336FC8B3B6A475">Expansion</option>

                                    <option value="403E58B055B54C27A9683BE935BF8033">General</option>

                                    <option value="D66A1BAB19FC44AA9E4B0E30AB49FF37">Innovation</option>

                                    <option value="DA4CDA2C3F0E48D7A1748A9CA8B2CF39">Keel laying</option>

                                    <option value="897BF50704934D7297851284DCF44FBF">Maintenance</option>

                                    <option value="0E5AF435BA114CFCBDF9E253E09831AB">Naming ceremony</option>

                                    <option value="12C9F258A9C64B45B47D994C129CC917">New design</option>

                                    <option value="0F17712009CC4DF8BAF6EF5F678B6E9E">Online contract</option>

                                    <option value="995C287F1E18420D9245EF1A9D9B69A3">Partnership</option>

                                    <option value="75D527FF2CD34995A40F0590DDD6B84D">Product introduction</option>

                                    <option value="00ACBB5823C84855B1BE5416B8ADA0F0">Product Update</option>

                                    <option value="433ABAD3AD0C4D2FBCD78AE9EC699860">Project start</option>

                                    <option value="8D1E0043A41F417CB801D5F635B7CA81">Project update</option>

                                    <option value="AC2AC511CBA3415B96BFDF1017698CD6">Refit</option>

                                    <option value="B0624A2F12B04B75ADC31C45852156FC">Repair</option>

                                    <option value="38D53B6CCEBD454D9EBFC633D1A82C1D">Repair & Maintenance</option>

                                    <option value="21E6EDBF9DC84B09958C8C4AB74F7707">Stock</option>

                                    <option value="502B4B1527B240408FCEA1D437D41A8E">Upgrade</option>

                                    <option value="4ECB96F2FBF2412E9DDEE753CE569713">Website</option>

                                </select>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>


    <section class="grid12 listing listing-news">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <article>
                        <ul>
                            <li>
                                <a href="<?php echo base_url();?>index.php/home/news_detail">
                                    <img src='<?php echo base_url(); ?>asset/front/img/news/news-ship-1.jpg' />
                                </a>
                                <div>
                                    <h3><a href="<?php echo base_url();?>index.php/home/news_detail">Damen, Adriatic Marinas and the Montenegrin Government sign contract for redevelopment of Bijela shipyard</a></h3>
                                    <p class="date">30 November 2018</p>
                                    <p>The consortium of superyacht marina development and operating group Adriatic Marinas and the global ship and superyacht builder Damen, have been working with the Government of Montenegro.</p>
                                    <a href="<?php echo base_url();?>index.php/home/news_detail" class="cta">Read more &rsaquo;</a>
                                    <p class="tags"></p>
                                </div>
                            </li>

                            <li>
                                <a href="<?php echo base_url();?>index.php/home/news_detail">
                                    <img src='<?php echo base_url(); ?>asset/front/img/news/news-ship-2.jpg' />
                                </a>
                                <div>
                                    <h3><a href="<?php echo base_url();?>index.php/home/news_detail">Naviera Integral signs contract with Damen for its 15th Fast Crew Supplier FCS 5009</a></h3>
                                    <p class="date">29 November 2018</p>
                                    <p>Leading Mexican offshore contractor Naviera Integral has signed a contract with the Damen Shipyards Group for the delivery of a new Fast Crew Supplier FCS 5009 for delivery in 2019.</p>
                                    <a href="<?php echo base_url();?>index.php/home/news_detail" class="cta">Read more &rsaquo;</a>
                                    <p class="tags">
                                        <a href="<?php echo base_url();?>index.php/home/news_detail">Crew Supply Vessel</a>
                                    </p>
                                </div>
                            </li>

                            <li>
                                <a href="<?php echo base_url();?>index.php/home/news_detail">
                                    <img src='<?php echo base_url(); ?>asset/front/img/news/news-ship-3.jpg' />
                                </a>
                                <div>
                                    <h3><a href="<?php echo base_url();?>index.php/home/news_detail">Société Nouvelle de Remorquage et de Travaux Maritimes signs contract with Damen Shipyards Group</a></h3>
                                    <p class="date">29 November 2018</p>
                                    <p>Damen Shipyards Group has recently signed a contract with Société de Remorquage et de Travaux Maritimes (SNRTM) for the delivery of one Damen STu 1606 that shall operate in the Mediterranean Sea in Toulon, France.</p>
                                    <a href="<?php echo base_url();?>index.php/home/news_detail" class="cta">Read more &rsaquo;</a>
                                    <p class="tags">
                                        <a href="<?php echo base_url();?>index.php/home/news_detail">Sea Tug</a>
                                        <a href="<?php echo base_url();?>index.php/home/news_detail">Harbour Tug</a>
                                    </p>
                                </div>
                            </li>

                            <li>
                                <a href="<?php echo base_url();?>index.php/home/news_detail">
                                    <img src='<?php echo base_url(); ?>asset/front/img/news/news-ship-1.jpg' />
                                </a>
                                <div>
                                    <h3><a href="<?php echo base_url();?>index.php/home/news_detail">Damen holds another successful seminar in Brisbane, Australia</a></h3>
                                    <p class="date">27 November 2018</p>
                                    <p>Damen Services Brisbane, the Queensland Service Hub for the international Damen Shipyards Group, has hosted another successful seminar.</p>
                                    <a href="<?php echo base_url();?>index.php/home/news_detail" class="cta">Read more &rsaquo;</a>
                                    <p class="tags">
                                    </p>
                                </div>
                            </li>

                            <li>
                                <a href="<?php echo base_url();?>index.php/home/news_detail">
                                    <img src='<?php echo base_url(); ?>asset/front/img/news/news-ship-1.jpg' />
                                </a>
                                <div>
                                    <h3><a href="<?php echo base_url();?>index.php/home/news_detail">Proud to be part of naval cooperation</a></h3>
                                    <p class="date">27 November 2018</p>
                                    <p>Over the last six years, Damen has achieved the building of eight (out of a total of ten) Coastal Patrol Tenochtitlan class vessels (Damen SPa4207), one Logistic Support Vessel (Damen FCS5009) and one Long Range Ocean Patrol vessel POLA (Damen SIGMA 10514).</p>
                                    <a href="<?php echo base_url();?>index.php/home/news_detail" class="cta">Read more &rsaquo;</a>
                                    <p class="tags">
                                        <a href="<?php echo base_url();?>index.php/home/news_detail">Crew Supply Vessel</a>
                                        <a href="<?php echo base_url();?>index.php/home/news_detail">Stan Patrol</a>
                                    </p>
                                </div>
                            </li>

                            <li>
                                <a href="<?php echo base_url();?>index.php/home/news_detail">
                                    <img src='<?php echo base_url(); ?>asset/front/img/news/news-ship-3.jpg' />
                                </a>
                                <div>
                                    <h3><a href="<?php echo base_url();?>index.php/home/news_detail">Damen holds surplus material auction as part of sustainability drive</a></h3>
                                    <p class="date">26 November 2018</p>
                                    <p>Damen Shipyards Group’s commitment to sustainability sees the company regularly considering new initiatives that can improve its environmental performance.</p>
                                    <a href="<?php echo base_url();?>index.php/home/news_detail" class="cta">Read more &rsaquo;</a>
                                    <p class="tags">
                                    </p>
                                </div>
                            </li>

                            <li>
                                <a href="<?php echo base_url();?>index.php/home/news_detail">
                                    <img src='<?php echo base_url(); ?>asset/front/img/news/news-ship-1.jpg' />
                                </a>
                                <div>
                                    <h3><a href="<?php echo base_url();?>index.php/home/news_detail">Damen announces new service accommodation and transfer vessel</a></h3>
                                    <p class="date">26 November 2018</p>
                                    <p>Damen Shipyards Group has recently announced a new design. The Fast Crew Supplier (FCS) 3410 service accommodation and transfer vessel (SATV) contains numerous design features that ensure its suitability for operations in the developing offshore wind market in North America.</p>
                                    <a href="<?php echo base_url();?>index.php/home/news_detail" class="cta">Read more &rsaquo;</a>
                                    <p class="tags">
                                        <a href="<?php echo base_url();?>index.php/home/news_detail">Crew Supply Vessel</a>
                                    </p>
                                </div>
                            </li>

                            <li>
                                <a href="<?php echo base_url();?>index.php/home/news_detail">
                                    <img src='<?php echo base_url(); ?>asset/front/img/news/news-ship-2.jpg' />
                                </a>
                                <div>
                                    <h3><a href="<?php echo base_url();?>index.php/home/news_detail">Both floating docks of Damen Shiprepair Curaçao operational</a></h3>
                                    <p class="date">23 November 2018</p>
                                    <p>On the 22nd of November Damen Shiprepair Curaçao (DSCu), commissioned its floating D-dock.</p>
                                    <a href="<?php echo base_url();?>index.php/home/news_detail" class="cta">Read more &rsaquo;</a>
                                    <p class="tags">
                                    </p>
                                </div>
                            </li>

                            <li>
                                <a href="<?php echo base_url();?>index.php/home/news_detail">
                                    <img src='<?php echo base_url(); ?>asset/front/img/news/news-ship-3.jpg' />
                                </a>
                                <div>
                                    <h3><a href="<?php echo base_url();?>index.php/home/news_detail">Albwardy Damen celebrates 40th anniversary</a></h3>
                                    <p class="date">21 November 2018</p>
                                    <p>Albwardy Damen celebrated its 40th anniversary on the 15th of November by officially opening its new ship repair facility at Dubai Maritime City (DMC).</p>
                                    <a href="<?php echo base_url();?>index.php/home/news_detail" class="cta">Read more &rsaquo;</a>
                                    <p class="tags">
                                    </p>
                                </div>
                            </li>

                            <li>
                                <a href="<?php echo base_url();?>index.php/home/news_detail">
                                    <img src='<?php echo base_url(); ?>asset/front/img/news/news-ship-1.jpg' />
                                </a>
                                <div>
                                    <h3><a href="<?php echo base_url();?>index.php/home/news_detail">Damen Shiprepair Vlissingen supporting construction of Offshore Substation for Deutsche Bucht offshore wind farm</a></h3>
                                    <p class="date">21 November 2018</p>
                                    <p>Ten months after the final assembly of the offshore substation (OSS) for the new Deutsche Bucht offshore wind farm started at Damen Shiprepair Vlissingen (DSV), the substation topside has now been moved outside the construction hall so that further outfitting can take place.</p>
                                    <a href="<?php echo base_url();?>index.php/home/news_detail" class="cta">Read more &rsaquo;</a>
                                    <p class="tags">
                                    </p>
                                </div>
                            </li>
                        </ul>
                    </article>
                    <div class="showmore-buttons">
                        <div class="showmore show news-items"><span>Show more results</span></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>