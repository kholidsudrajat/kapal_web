<section class="page-head page-bg" style="background-image: url('<?php echo base_url(); ?>asset/front/img/slides/flexslider/2.jpg')">
    <div class="container">
        <h3 class="page-heading">DAMEN, ADRIATIC MARINAS AND THE MONTENEGRIN GOVERNMENT SIGN CONTRACT FOR REDEVELOPMENT OF BIJELA SHIPYARD</h3>
        <div class="sub-title">
            <a href="<?php echo base_url(); ?>"><span>Home</span></a>
            <i class="fa fa-angle-right"></i>
            <span>Articles</span>
        </div>
    </div>
</section>
<section class="detail detail-news" style="padding: 0 0 20px; border-bottom: 1px solid #ccc;">
    <article class="detail-content">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="grid">
                        <div class="grid12">
                            <p class="date">30 November 2018</p>
                            <div class="user-generated">
                                <style>
                                    .image01 {
                                        WIDTH: 40% !important;
                                        FLOAT: right
                                    }

                                    @media Unknown {
                                        .image01 {
                                            WIDTH: 100% !important;
                                            FLOAT: right
                                        }
                                        .image01 {
                                            WIDTH: 100% !important;
                                            FLOAT: right
                                        }
                                    }
                                </style>
                                <p><strong>Consortium to transform site into superyacht repair and refit facility</strong></p>
                                <p><strong>The consortium of superyacht marina development and operating group Adriatic Marinas and the global ship and superyacht builder Damen, have been working with the Government of Montenegro. Since 2012 the parties have been developing plans to bring about the redevelopment of the former Bijela shipyard in Boka Bay, Montenegro.</strong></p>
                                <p>The recent completion of technical surveys and the placing of a contract by the government and the World Bank for the remediation of the site has paved the way for the signing of the Concession Agreement for the yard on 30th November. Following completion of the remediation works in early 2020, it will be transformed by the consortium over the following 12 months into a superyacht repair and refit facility.</p>
                                <p>This initiative has been challenging, but the resulting benefits to the economy of Montenegro are potentially significant with over 300 people eventually expected to return to the marine industry. Bijela, Herzeg Novi, is the historic homeland of the sector in Montenegro and with this the region will once again deliver Montenegro&rsquo;s renowned skills to the Mediterranean marine market.</p>

                                <div class="image01" style="margin-left: 10px;"><a href="<?php echo base_url();?>index.php/home/news" target="_blank"><img height="767" alt="Bijela shipyard" width="1300" src="<?php echo base_url(); ?>asset/front/img/news/news-ship-1.jpg" /></a></div>

                                <p>The planning of recruitment and training through the reputable apprenticeship programme that Damen operates throughout its global network of shipyards is already underway. So too are plans for the production of new, state-of-the-art lifting and repair equipment to be brought to the facility following completion of remediation works.</p>
                                <p>In the meantime, it is hoped that interim arrangements can be agreed with the Government of Montenegro and its remediation contractor Valgo, to allow a preliminary yachting service to be operated in the western portion of the Bijela site. This could commence as early as winter 2019/20.</p>
                                <p>Rene Berkvens, Damen&rsquo;s CEO, commented: &ldquo;We are happy that today marks the beginning of a new lease of life for the shipyard in Bijela. Together with our partners we take a long-term view on making yacht maintenance and refit in Montenegro a success. A strong customer base at Porto Montenegro and yacht maintenance and refit skills within the Damen Group will contribute to the success of this venture. Considering the yard&rsquo;s current condition, we face a challenging period. However, with the continuous full support of the Montenegrin Government, and in close cooperation with our strong partner and the local community, we aim to restore the yard&rsquo;s health, increase employment, and pursue fitting commercial opportunities both locally and internationally that will contribute to rebuilding activity levels.&rdquo;</p>
                                <p>Adriatic Marinas Managing Director David Margason stated: &ldquo;The teams at Adriatic Marinas and Damen have been working for a very long time with the Government of Montenegro to realize this challenging and exciting project. We are delighted that this next major step has been achieved and believe that this initiative is an ideal example of how Montenegro&rsquo;s fast-growing tourism and marine leisure industry can convert back into the generation of jobs in associated creative and production industries to offer a comprehensive, integrated and sustainable range of employment opportunities to future generations in the country.&rdquo;</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </article>
</section>