<section class="portfolios section port-detail">
    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-xs-12 col-sm-offset-2 col-xs-offset-0">
                <div class="section-heading">
                    <h2 class="title"><span>Our</span> Gallery</h2>
                    <p>.</p>
                </div>
            </div>
        </div>
        <!-- /.row -->
        <!--<div class="portfolio-filter">
            <a class="current" href="#" data-filter="*">All</a>
        </div>-->
        
        <div class="row">
            <div class="portfolio-details">
                <div class="portfolio-container grid">
                    <div class="grid-item col-md-4 col-xs-6 figts limited">
                        <div class="portfolio-item">
                            <img class="img-responsive" src='<?php echo base_url(); ?>asset/front/img/img_galeri/202574DSC_6886.JPG' alt="Gallery">
                            <div class="port-hover">
                                <div class="port-content">
                                    <a href="#"><h3>Activity</h3></a>
                                    <p>GLAD - February 2018</p>
                                    <a href='<?php echo base_url(); ?>asset/front/img/img_galeri/202574DSC_6886.JPG' data-fancybox="group" class="port-icon">
                                        <img class="hidden-thumbnail" src='<?php echo base_url(); ?>asset/front/img/img_galeri/202574DSC_6886.JPG' alt="Thumbnail">
                                        <img src='<?php echo base_url(); ?>asset/front/img/icons/plus-btn.png' alt="Icon">
                                    </a>
                                </div>
                                <!-- /.port-content -->
                            </div>
                            <!-- /.port-hover -->
                        </div>
                        <!-- /.portfolio-item -->
                    </div>
                    <!-- /.grid-item -->
                    <div class="grid-item col-md-4 col-xs-6 figts limited">
                        <div class="portfolio-item">
                            <img class="img-responsive" src='<?php echo base_url(); ?>asset/front/img/img_galeri/202985DSC_6813.JPG' alt="Gallery">
                            <div class="port-hover">
                                <div class="port-content">
                                    <a href="#"><h3>Activity</h3></a>
                                    <p>BKI-PINDAD Mou</p>
                                    <a href='<?php echo base_url(); ?>asset/front/img/img_galeri/202985DSC_6813.JPG' data-fancybox="group" class="port-icon">
                                        <img class="hidden-thumbnail" src='<?php echo base_url(); ?>asset/front/img/img_galeri/202985DSC_6813.JPG' alt="Thumbnail">
                                        <img src='<?php echo base_url(); ?>asset/front/img/icons/plus-btn.png' alt="Icon">
                                    </a>
                                </div>
                                <!-- /.port-content -->
                            </div>
                            <!-- /.port-hover -->
                        </div>
                        <!-- /.portfolio-item -->
                    </div>
                    <!-- /.grid-item -->
                    <div class="grid-item col-md-4 col-xs-6 figts limited">
                        <div class="portfolio-item">
                            <img class="img-responsive" src='<?php echo base_url(); ?>asset/front/img/img_galeri/769839DSC_6365.JPG' alt="Gallery">
                            <div class="port-hover">
                                <div class="port-content">
                                    <a href="#"><h3>Activity</h3></a>
                                    <p>Gala Dinner</p>
                                    <a href='<?php echo base_url(); ?>asset/front/img/img_galeri/769839DSC_6365.JPG' data-fancybox="group" class="port-icon">
                                        <img class="hidden-thumbnail" src='<?php echo base_url(); ?>asset/front/img/img_galeri/769839DSC_6365.JPG' alt="Thumbnail">
                                        <img src='<?php echo base_url(); ?>asset/front/img/icons/plus-btn.png' alt="Icon">
                                    </a>
                                </div>
                                <!-- /.port-content -->
                            </div>
                            <!-- /.port-hover -->
                        </div>
                        <!-- /.portfolio-item -->
                    </div>
                    <!-- /.grid-item -->
                    <div class="grid-item col-md-4 col-xs-6 figts limited">
                        <div class="portfolio-item">
                            <img class="img-responsive" src='<?php echo base_url(); ?>asset/front/img/img_galeri/872522.jpeg' alt="Gallery">
                            <div class="port-hover">
                                <div class="port-content">
                                    <a href="#"><h3>Activity</h3></a>
                                    <p>Statutory Survey Socialization</p>
                                    <a href='<?php echo base_url(); ?>asset/front/img/img_galeri/872522.jpeg' data-fancybox="group" class="port-icon">
                                        <img class="hidden-thumbnail" src='<?php echo base_url(); ?>asset/front/img/img_galeri/872522.jpeg' alt="Thumbnail">
                                        <img src='<?php echo base_url(); ?>asset/front/img/icons/plus-btn.png' alt="Icon">
                                    </a>
                                </div>
                                <!-- /.port-content -->
                            </div>
                            <!-- /.port-hover -->
                        </div>
                        <!-- /.portfolio-item -->
                    </div>
                    <!-- /.grid-item -->
                    <div class="grid-item col-md-4 col-xs-6 figts limited">
                        <div class="portfolio-item">
                            <img class="img-responsive" src='<?php echo base_url(); ?>asset/front/img/img_galeri/63059.jpeg' alt="Gallery">
                            <div class="port-hover">
                                <div class="port-content">
                                    <a href="#"><h3>Activity</h3></a>
                                    <p>Ship Launching</p>
                                    <a href='<?php echo base_url(); ?>asset/front/img/img_galeri/63059.jpeg' data-fancybox="group" class="port-icon">
                                        <img class="hidden-thumbnail" src='<?php echo base_url(); ?>asset/front/img/img_galeri/63059.jpeg' alt="Thumbnail">
                                        <img src='<?php echo base_url(); ?>asset/front/img/icons/plus-btn.png' alt="Icon">
                                    </a>
                                </div>
                                <!-- /.port-content -->
                            </div>
                            <!-- /.port-hover -->
                        </div>
                        <!-- /.portfolio-item -->
                    </div>
                    <!-- /.grid-item -->
                    <div class="grid-item col-md-4 col-xs-6 figts limited">
                        <div class="portfolio-item">
                            <img class="img-responsive" src='<?php echo base_url(); ?>asset/front/img/img_galeri/855394.jpeg' alt="Gallery">
                            <div class="port-hover">
                                <div class="port-content">
                                    <a href="#"><h3>CSR</h3></a>
                                    <p>Social Responsibility</p>
                                    <a href='<?php echo base_url(); ?>asset/front/img/img_galeri/855394.jpeg' data-fancybox="group" class="port-icon">
                                        <img class="hidden-thumbnail" src='<?php echo base_url(); ?>asset/front/img/img_galeri/855394.jpeg' alt="Thumbnail">
                                        <img src='<?php echo base_url(); ?>asset/front/img/icons/plus-btn.png' alt="Icon">
                                    </a>
                                </div>
                                <!-- /.port-content -->
                            </div>
                            <!-- /.port-hover -->
                        </div>
                        <!-- /.portfolio-item -->
                    </div>
                    <!-- /.grid-item -->
                    <div class="grid-item col-md-4 col-xs-6 figts limited">
                        <div class="portfolio-item">
                            <img class="img-responsive" src='<?php echo base_url(); ?>asset/front/img/img_galeri/122517DSC_5745.JPG' alt="Gallery">
                            <div class="port-hover">
                                <div class="port-content">
                                    <a href="#"><h3>Activity</h3></a>
                                    <p>Seminar of IMO Meeting Result</p>
                                    <a href='<?php echo base_url(); ?>asset/front/img/img_galeri/122517DSC_5745.JPG' data-fancybox="group" class="port-icon">
                                        <img class="hidden-thumbnail" src='<?php echo base_url(); ?>asset/front/img/img_galeri/122517DSC_5745.JPG' alt="Thumbnail">
                                        <img src='<?php echo base_url(); ?>asset/front/img/icons/plus-btn.png' alt="Icon">
                                    </a>
                                </div>
                                <!-- /.port-content -->
                            </div>
                            <!-- /.port-hover -->
                        </div>
                        <!-- /.portfolio-item -->
                    </div>
                    <!-- /.grid-item -->
                    <div class="grid-item col-md-4 col-xs-6 figts limited">
                        <div class="portfolio-item">
                            <img class="img-responsive" src='<?php echo base_url(); ?>asset/front/img/img_galeri/135580DSC_5628.JPG' alt="Gallery">
                            <div class="port-hover">
                                <div class="port-content">
                                    <a href="#"><h3>Activity</h3></a>
                                    <p>Inauguration of The Commissioner</p>
                                    <a href='<?php echo base_url(); ?>asset/front/img/img_galeri/135580DSC_5628.JPG' data-fancybox="group" class="port-icon">
                                        <img class="hidden-thumbnail" src='<?php echo base_url(); ?>asset/front/img/img_galeri/135580DSC_5628.JPG' alt="Thumbnail">
                                        <img src='<?php echo base_url(); ?>asset/front/img/icons/plus-btn.png' alt="Icon">
                                    </a>
                                </div>
                                <!-- /.port-content -->
                            </div>
                            <!-- /.port-hover -->
                        </div>
                        <!-- /.portfolio-item -->
                    </div>
                    <!-- /.grid-item -->
                    <div class="grid-item col-md-4 col-xs-6 figts limited">
                        <div class="portfolio-item">
                            <img class="img-responsive" src='<?php echo base_url(); ?>asset/front/img/img_galeri/4411DSC_5063.JPG' alt="Gallery">
                            <div class="port-hover">
                                <div class="port-content">
                                    <a href="#"><h3>Activity</h3></a>
                                    <p>Campus Visits</p>
                                    <a href='<?php echo base_url(); ?>asset/front/img/img_galeri/4411DSC_5063.JPG' data-fancybox="group" class="port-icon">
                                        <img class="hidden-thumbnail" src='<?php echo base_url(); ?>asset/front/img/img_galeri/4411DSC_5063.JPG' alt="Thumbnail">
                                        <img src='<?php echo base_url(); ?>asset/front/img/icons/plus-btn.png' alt="Icon">
                                    </a>
                                </div>
                                <!-- /.port-content -->
                            </div>
                            <!-- /.port-hover -->
                        </div>
                        <!-- /.portfolio-item -->
                    </div>
                    <!-- /.grid-item -->
                </div>
                <!-- /.portfolio-container -->
            </div>
            <!-- /.portfolio-details -->
        </div>
        
    </div>
</section>