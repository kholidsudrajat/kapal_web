<div class="page-in section">
    <div class="container">
        <div class="row">
            <div class="blog">
                <div class="col-xs-12 col-sm-12 col-md-8">
                    <div class="blog-content">
                        <div class="post-title">
                            <a href="single.html">
                                <h3>Vision and Mission</h3>
                            </a>
                        </div>
                        <div class="post-content">
                            <section>
                                <h3 style="text-align: justify;"><strong>Visions 2016 -2020:</strong></h3>
                                <p style="text-align: justify;">To become a world-class classification society and independent assurance provider</p>
                                <h3 style="text-align: justify;"><strong>Missions&nbsp;</strong><strong>2016 -2020:</strong></h3>
                                <ol>
                                    <li style="text-align: justify;">To provide the best possible added value to customers of the classificaiton and statutory services through international standard handling, operation, and rules research, in terms of quality, safety and social responsibility as well as responsibility towards the marine environment (classification).</li>
                                    <li style="text-align: justify;">To maximize the resources of BKI towards its full potential to become the market leader in the independent marine assurance business (nonclassification).</li>
                                </ol>
                                <p style="text-align: justify;">&nbsp;</p>
                            </section>
                            <p>&nbsp;</p>
                            <p>&nbsp;</p>
                            <p>&nbsp;</p>
                        </div>
                    </div>
                    <!-- /.blog-content -->
                </div>
                <!-- /.col- -->
            </div>
            <!-- /.blog -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</div>