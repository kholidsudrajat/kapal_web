<h4 class="heading">Achievement</h4>
<div id="grid-container" class="cbp-l-grid-projects">
    <ul>
        <li class="cbp-item graphic ">
            <div class="cbp-caption">
                <div class="cbp-caption-defaultWrap">
                    <img height="100%" width="100%" src="<?php echo base_url(); ?>asset/front/img/achievement/achievement2.jpg" alt="" />
                </div>
                <div class="cbp-caption-activeWrap">
                    <div class="cbp-l-caption-alignCenter">
                        <div class="cbp-l-caption-body">
                            <a href="<?php echo base_url(); ?>asset/front/img/achievement/achievement2.jpg" class="cbp-lightbox cbp-l-caption-buttonRight" data-title="Dashboard<br>by Paul Flavius Nechita">view larger</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="cbp-l-grid-projects-title">Achievement from awards 2019</div>
            <div class="cbp-l-grid-projects-desc">
                <p>
                        Consetetur sadipscing elitr, sed diam non mod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. 
                        Consetetur sadipscing elitr, sed diam non mod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
                </p>
            </div>
        </li>
        <li class="cbp-item graphic ">
            <div class="cbp-caption">
                <div class="cbp-caption-defaultWrap">
                    <img height="100%" width="100%" src="<?php echo base_url(); ?>asset/front/img/achievement/achievement3.jpg" alt="" />
                </div>
                <div class="cbp-caption-activeWrap">
                    <div class="cbp-l-caption-alignCenter">
                        <div class="cbp-l-caption-body">
                            <a href="<?php echo base_url(); ?>asset/front/img/achievement/achievement3.jpg" class="cbp-lightbox cbp-l-caption-buttonRight" data-title="Dashboard<br>by Paul Flavius Nechita">view larger</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="cbp-l-grid-projects-title">Achievement from awards 2019</div>
            <div class="cbp-l-grid-projects-desc">
                <p>
                        Consetetur sadipscing elitr, sed diam non mod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. 
                        Consetetur sadipscing elitr, sed diam non mod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
                </p>
            </div>
        </li>
        <li class="cbp-item graphic ">
            <div class="cbp-caption">
                <div class="cbp-caption-defaultWrap">
                    <img height="100%" width="100%" src="<?php echo base_url(); ?>asset/front/img/achievement/achievement4.jpg" alt="" />
                </div>
                <div class="cbp-caption-activeWrap">
                    <div class="cbp-l-caption-alignCenter">
                        <div class="cbp-l-caption-body">
                            <a href="<?php echo base_url(); ?>asset/front/img/achievement/achievement4.jpg" class="cbp-lightbox cbp-l-caption-buttonRight" data-title="Dashboard<br>by Paul Flavius Nechita">view larger</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="cbp-l-grid-projects-title">Achievement from awards 2019</div>
            <div class="cbp-l-grid-projects-desc">
                <p>
                        Consetetur sadipscing elitr, sed diam non mod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. 
                        Consetetur sadipscing elitr, sed diam non mod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
                </p>
            </div>
        </li>
        <li class="cbp-item graphic ">
            <div class="cbp-caption">
                <div class="cbp-caption-defaultWrap">
                    <img height="100%" width="100%" src="<?php echo base_url(); ?>asset/front/img/achievement/achievement6.jpg" alt="" />
                </div>
                <div class="cbp-caption-activeWrap">
                    <div class="cbp-l-caption-alignCenter">
                        <div class="cbp-l-caption-body">
                            <a href="<?php echo base_url(); ?>asset/front/img/achievement/achievement6.jpg" class="cbp-lightbox cbp-l-caption-buttonRight" data-title="Dashboard<br>by Paul Flavius Nechita">view larger</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="cbp-l-grid-projects-title">Achievement from awards 2019</div>
            <div class="cbp-l-grid-projects-desc">
                <p>
                        Consetetur sadipscing elitr, sed diam non mod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. 
                        Consetetur sadipscing elitr, sed diam non mod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
                </p>
            </div>
        </li>
    </ul>
</div>

<div class="cbp-l-loadMore-button">
    <a href="#" class="cbp-l-loadMore-button-link">LOAD MORE</a>
</div>