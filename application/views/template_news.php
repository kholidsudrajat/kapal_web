<!DOCTYPE html>
<html lang="en">
<?php require_once('frontend/temp_front/head.php'); ?>

<body>
    <div id="wrapper">
        <!-- start header -->
        <?php require_once('frontend/temp_front/header.php'); ?>
        <!-- end header -->
        <!--Start Content -->
        <?php echo $contents; ?>
        <!--End Content-->

    </div>
    <!-- Start Footer -->
    <?php require_once('frontend/temp_front/footer.php'); ?>
    <!-- End Footer -->
    <a href="#" class="back-to-top"><i class="fa fa-angle-up"></i></a>
    <!--Start js-->
    <?php require_once('frontend/temp_front/js.php'); ?>
    <!--End js-->
</body>

</html>